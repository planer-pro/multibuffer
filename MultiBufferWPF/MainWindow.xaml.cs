﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MultiBufferLibr;

namespace MultiBufferWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Logic clipboardManager = new Logic();

        public MainWindow()
        {
            InitializeComponent();
            clipboardManager.ClipboardChangedEvent += ClipboardChange;
            ClipboardChange(clipboardManager.GetClipbList());
            //window1.Background = new SolidColorBrush { Opacity = 0 };


            window1.Left = System.Windows.SystemParameters.PrimaryScreenWidth - window1.Width + 40;
            window1.Top = System.Windows.SystemParameters.PrimaryScreenHeight - window1.Height;

        }

        private void ClipboardChange(List<string> cliplist)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                comboBox.Items.Clear();
                foreach (var item in cliplist)
                {
                    comboBox.Items.Add(item);
                }
                comboBox.SelectedIndex = comboBox.Items.Count - 1;
            }));
        }



        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (comboBox.SelectedItem == null) return;
            //dontChange = true;
            clipboardManager.SetItemToClipb(comboBox.SelectedItem.ToString());

        }

        private void comboBox_MouseEnter(object sender, MouseEventArgs e)
        {
            comboBox.Opacity = 1;
        }

        private void comboBox_MouseLeave(object sender, MouseEventArgs e)
        {
            comboBox.Opacity = 0.3;
        }

        private void comboBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            clipboardManager.ClearAllList();
            comboBox.Items.Clear();
            clipboardManager.SetItemToClipb("");
        }

        private void comboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                clipboardManager.RemoveItemFromList(comboBox.SelectedItem.ToString());
                comboBox.Items.RemoveAt(comboBox.SelectedIndex);
                clipboardManager.SetItemToClipb("");

            }
        }
    }
}
