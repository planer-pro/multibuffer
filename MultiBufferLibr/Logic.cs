﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MultiBufferLibr
{
    public delegate void ClipboardChangedEventHandler(List<string> clipList);

    public class Logic
    {
        private string lastText = null;
        private Thread myThread;

     
        public event ClipboardChangedEventHandler ClipboardChangedEvent;

        public Logic()
        {
            myThread = new Thread(new ThreadStart(MainLogic));
            myThread.SetApartmentState(ApartmentState.STA);
            myThread.Start();
        }

        public string GetClipbToText()
        {
            if (Clipboard.ContainsText(TextDataFormat.Text))
            {
                String returnClibToText = null;


                try
                {
                    returnClibToText = Clipboard.GetText(TextDataFormat.Text);
                }
                catch (Exception)
                {
                    Debug.WriteLine("seems accured incoming clopboard error))");
                }

                //todo сделать конвертацию приходящего стринга в кодировку браузера 

                if (lastText != returnClibToText)
                {
                    lastText = returnClibToText;

                    return returnClibToText;
                }

            }
            return null;
        }

        List<string> clipbList = new List<string>();
        public void AddItemToList(string item)
        {
            clipbList.Add(item);
        }

        public void RemoveItemFromList(string item)
        {
            clipbList.Remove(item);
        }

        public void ClearAllList()
        {
            clipbList.Clear();
        }

        public void SetItemToClipb(string item)
        {
            lastText = item;
            Clipboard.SetText(item, TextDataFormat.Text);
        }

        public List<string> GetClipbList()
        {
            return clipbList;
        }

        private void MainLogic()
        {
            while (true)
            {
                string currentText = GetClipbToText();

               if (currentText != null)
                {
                    AddItemToList(currentText);
                    if (ClipboardChangedEvent != null)
                    ClipboardChangedEvent(GetClipbList());
                }
                Thread.Sleep(100);
            }
        }
    }
}
