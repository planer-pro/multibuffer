﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiBuffer
{
    class Program
    {
        [STAThreadAttribute]
        static void Main(string[] args)
        {
            
            ClipboardManager clipboardManager = new ClipboardManager();

            while (true)
            {
                string currentText = clipboardManager.GetClipbToText();
                if (currentText != null)
                {
                    clipboardManager.AddItemToList(currentText);
                    foreach (var item in clipboardManager.GetClipbList())
                    {  
                        Console.WriteLine(item);
                    }
                }         
                Thread.Sleep(100);
            }
        }

    }
}
