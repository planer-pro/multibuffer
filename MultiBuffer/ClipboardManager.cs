﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiBuffer
{
    class ClipboardManager
    {
        private string lastText = null;
        public string GetClipbToText()
        {
            if (Clipboard.ContainsText(TextDataFormat.Text))
            {
                String returnClibToText = null;

                returnClibToText = Clipboard.GetText(TextDataFormat.Text);
                if (lastText != returnClibToText)
                {
                    lastText = returnClibToText;

                    return returnClibToText;
                }
                
            }
            return null;
        }

        List<string> clipbList = new List<string>();
        public void AddItemToList(string item)
        {
            clipbList.Add(item);
        }

        public void RemoveItemFromList(string item)
        {
            clipbList.Remove(item);
        }

        public void ClearAllList()
        {
            clipbList.Clear();
        }

        public void SetItemToClipb(string item)
        {
            Clipboard.SetText(item, TextDataFormat.Text);
        }

        public List<string> GetClipbList()
        {
            return clipbList;
        }

    }
}
